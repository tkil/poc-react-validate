import React from "react";

import { UserForm } from "@components/userForm/userForm";

import "./app.css";

interface IProps {
  propData?: string;
}

const defaultProps: IProps = {
  propData: "",
};

export const App: React.FC<IProps> = (props: IProps = defaultProps) => {
  return (
    <div className="app" role="none">
      -- App Component --
      <UserForm />
    </div>
  );
};
