import React from "react";

import "./userForm.css"

interface IProps {
  propData?: string;
}

const defaultProps: IProps = {
  propData: "",
}

export const UserForm: React.FC<IProps> = (props: IProps = defaultProps) => {
  return <div className="user-form" role="none" >
    -- UserForm Component --
  </div>;
}
