// NPM
import React from "react";
import { render } from "@testing-library/react";

// Project
import { UserForm } from "../userForm";

describe("Component: UserForm", () => {
  describe("render()", () => {
    it("should render without error", () => {
      // Arrange
      const { baseElement } = render(<UserForm />);
      // Act
      const element = baseElement.firstChild;
      // Assert
      expect(element).not.toBeNull();
    });
  });
});
